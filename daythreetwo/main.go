package main

import (
	ctrl "daythreetwo/controller"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.POST("/tambah", ctrl.Tambah)
	router.POST("/kurang", ctrl.Kurang)
	router.POST("/kali", ctrl.Kali)
	router.POST("/bagi", ctrl.Bagi)
	router.POST("/faktorial", ctrl.Faktorial)

	router.Run()
}
