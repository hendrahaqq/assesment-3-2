package controller

import (
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

// type Angka struct {
// 	Angka1  string `json:"angka1"`
// 	Angka2  string `json:"angka2"`
// 	Angka3  string `json:"angka3"`
// 	Angka4  string `json:"angka4"`
// 	Angka5  string `json:"angka5"`
// 	Angka6  string `json:"angka6"`
// 	Angka7  string `json:"angka7"`
// 	Angka8  string `json:"angka8"`
// 	Angka9  string `json:"angka9"`
// 	Angka10 string `json:"angka10"`
// }

// type Faktorial struct {
// 	Angka1 string `json:"angka1"`
// }

func Tambah(c *gin.Context) {
	angka1 := c.PostForm("angka1")
	angka2 := c.PostForm("angka2")
	angka3 := c.PostForm("angka3")
	angka4 := c.PostForm("angka4")
	angka5 := c.PostForm("angka5")
	angka6 := c.PostForm("angka6")
	angka7 := c.PostForm("angka7")
	angka8 := c.PostForm("angka8")
	angka9 := c.PostForm("angka9")
	angka10 := c.PostForm("angka10")

	angka1new, _ := strconv.Atoi(angka1)
	angka2new, _ := strconv.Atoi(angka2)
	angka3new, _ := strconv.Atoi(angka3)
	angka4new, _ := strconv.Atoi(angka4)
	angka5new, _ := strconv.Atoi(angka5)
	angka6new, _ := strconv.Atoi(angka6)
	angka7new, _ := strconv.Atoi(angka7)
	angka8new, _ := strconv.Atoi(angka8)
	angka9new, _ := strconv.Atoi(angka9)
	angka10new, _ := strconv.Atoi(angka10)

	result := angka1new + angka2new + angka3new + angka4new + angka5new + angka6new + angka7new + angka8new + angka9new + angka10new

	fmt.Println(result)
	c.JSON(200, gin.H{
		"hasil": result,
	})
}

func Kurang(c *gin.Context) {
	angka1 := c.PostForm("angka1")
	angka2 := c.PostForm("angka2")
	angka3 := c.PostForm("angka3")
	angka4 := c.PostForm("angka4")
	angka5 := c.PostForm("angka5")
	angka6 := c.PostForm("angka6")
	angka7 := c.PostForm("angka7")
	angka8 := c.PostForm("angka8")
	angka9 := c.PostForm("angka9")
	angka10 := c.PostForm("angka10")

	angka1new, _ := strconv.Atoi(angka1)
	angka2new, _ := strconv.Atoi(angka2)
	angka3new, _ := strconv.Atoi(angka3)
	angka4new, _ := strconv.Atoi(angka4)
	angka5new, _ := strconv.Atoi(angka5)
	angka6new, _ := strconv.Atoi(angka6)
	angka7new, _ := strconv.Atoi(angka7)
	angka8new, _ := strconv.Atoi(angka8)
	angka9new, _ := strconv.Atoi(angka9)
	angka10new, _ := strconv.Atoi(angka10)

	result := angka1new - angka2new - angka3new - angka4new - angka5new - angka6new - angka7new - angka8new - angka9new - angka10new

	fmt.Println(result)
	c.JSON(200, gin.H{
		"hasil": result,
	})
}
func Bagi(c *gin.Context) {

	angka1 := c.PostForm("angka1")
	angka2 := c.PostForm("angka2")
	angka3 := c.PostForm("angka3")
	angka4 := c.PostForm("angka4")
	angka5 := c.PostForm("angka5")
	angka6 := c.PostForm("angka6")
	angka7 := c.PostForm("angka7")
	angka8 := c.PostForm("angka8")
	angka9 := c.PostForm("angka9")
	angka10 := c.PostForm("angka10")

	angka1new, _ := strconv.Atoi(angka1)
	angka2new, _ := strconv.Atoi(angka2)
	angka3new, _ := strconv.Atoi(angka3)
	angka4new, _ := strconv.Atoi(angka4)
	angka5new, _ := strconv.Atoi(angka5)
	angka6new, _ := strconv.Atoi(angka6)
	angka7new, _ := strconv.Atoi(angka7)
	angka8new, _ := strconv.Atoi(angka8)
	angka9new, _ := strconv.Atoi(angka9)
	angka10new, _ := strconv.Atoi(angka10)

	result := angka1new / angka2new / angka3new / angka4new / angka5new / angka6new / angka7new / angka8new / angka9new / angka10new

	fmt.Println(result)
	c.JSON(200, gin.H{
		"hasil": result,
	})

}
func Kali(c *gin.Context) {

	angka1 := c.PostForm("angka1")
	angka2 := c.PostForm("angka2")
	angka3 := c.PostForm("angka3")
	angka4 := c.PostForm("angka4")
	angka5 := c.PostForm("angka5")
	angka6 := c.PostForm("angka6")
	angka7 := c.PostForm("angka7")
	angka8 := c.PostForm("angka8")
	angka9 := c.PostForm("angka9")
	angka10 := c.PostForm("angka10")

	angka1new, _ := strconv.Atoi(angka1)
	angka2new, _ := strconv.Atoi(angka2)
	angka3new, _ := strconv.Atoi(angka3)
	angka4new, _ := strconv.Atoi(angka4)
	angka5new, _ := strconv.Atoi(angka5)
	angka6new, _ := strconv.Atoi(angka6)
	angka7new, _ := strconv.Atoi(angka7)
	angka8new, _ := strconv.Atoi(angka8)
	angka9new, _ := strconv.Atoi(angka9)
	angka10new, _ := strconv.Atoi(angka10)

	result := angka1new * angka2new * angka3new * angka4new * angka5new * angka6new * angka7new * angka8new * angka9new * angka10new

	fmt.Println(result)
	c.JSON(200, gin.H{
		"hasil": result,
	})

}

func Faktorial(c *gin.Context) {
	angka := c.PostForm("angka")
	n, _ := strconv.Atoi(angka)
	var result uint64 = 1

	if n < 0 {
		fmt.Println("salah")
		result = 0
	} else {
		for i := 1; i <= n; i++ {
			result *= uint64(i)
		}

	}

	fmt.Println(result)
	c.JSON(200, gin.H{
		"hasil": result,
	})
}
